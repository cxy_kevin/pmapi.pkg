from pmapi import load_packages  # 自动运行
from pmapi import log
from pmapi.update import update
from pmapi.search import search
from pmapi.upgrade import upgrade
from pmapi.install import install
from pmapi.remove import remove
load_packages.do_nothing()
log.__log_start("--- log start ---")
