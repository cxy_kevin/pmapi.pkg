#    ------ cxykevin log moudle ------
# This moudle is not a part of this project.
# This file is CopyNone.


import logging
from pmapi import config
import sys
import os
levels = {
    "debug": logging.DEBUG,
    'info': logging.INFO,
    'warning': logging.WARN,
    'err': logging.ERROR
}


logging.basicConfig(filename=os.path.dirname(__file__)+os.sep+"log.log",
                    format='[%(asctime)s][%(levelname)s] %(message)s',
                    level=levels[config.LOGLEVEL], filemode="a")


def __log_start(msg: str) -> None:
    logging.info(msg)


def info(msg: str) -> None:
    if (20 >= levels[config.LOGLEVEL] and config.OUTPUT_STD):
        sys.stderr.write(
            ("[\033[34mINFO\033[0m]" if config.OUTPUT_COLORFUL else "[INFO]") + msg + "\n")
    logging.info(msg)


def warn(msg: str) -> None:
    if (30 >= levels[config.LOGLEVEL] and config.OUTPUT_STD):
        sys.stderr.write(
            ("[\033[33mWARNING\033[0m]" if config.OUTPUT_COLORFUL else "[WARNING]") + msg + "\n")
    logging.warn(msg)


def err(msg: str) -> None:
    if (40 >= levels[config.LOGLEVEL] and config.OUTPUT_STD):
        sys.stderr.write(
            ("[\033[31mERROR\033[0m]" if config.OUTPUT_COLORFUL else "[ERROR]") + msg + "\n")
    logging.error(msg)


def break_err(msg: str) -> None:
    if (40 >= levels[config.LOGLEVEL] and config.OUTPUT_STD):
        sys.stderr.write(logging
                         ("[\033[31mERROR\033[0m]" if config.OUTPUT_COLORFUL else "[ERROR]") + msg + "\n")
    logging.error(msg)


def print(*args, end="\n") -> None:
    msg = ' '.join(map(str, args))
    if (10 >= levels[config.LOGLEVEL]):
        sys.stdout.write(msg + end)
    logging.debug(msg)
